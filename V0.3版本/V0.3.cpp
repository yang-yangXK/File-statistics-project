﻿#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    string filename;
    char option;

    while (true) {
        cout << "Enter the option (c - characters, w - words, s - sentences, d - code lines, k - empty lines, z - comment lines, a - all, q - quit): ";
        cin >> option;

        if (option == 'q') {
            break; // 键入q即退出程序；
        }

        cout << "Enter the filename: ";
        cin >> filename;

        ifstream file(filename);

        if (!file) {
            cout << "Error: Unable to open the file." << endl;
            continue; // 如果无法打开文件，请继续执行下一次迭代；
        }

        string line;
        int charCount = 0;
        int wordCount = 0;
        int sentenceCount = 0;
        int codeLineCount = 0;
        int emptyLineCount = 0;
        int commentLineCount = 0;
        bool inCommentBlock = false;

        while (getline(file, line)) {
            if (line.empty()) {
                emptyLineCount++;
            }
            else {
                bool inCodeLine = true;

                for (size_t i = 0; i < line.length(); i++) {
                    char c = line[i];

                    charCount++;

                    if (c == ' ' || c == '\t') {
                        if (inCodeLine) {
                            wordCount++;
                        }
                        inCodeLine = false;
                    }
                    else if (c == '.' || c == '?' || c == '!') {
                        sentenceCount++;
                        if (inCodeLine) {
                            wordCount++;
                        }
                        inCodeLine = false;
                    }
                    else if (c == '/' && i + 1 < line.length()) {
                        if (line[i + 1] == '/' || (line[i + 1] == '*' && !inCommentBlock)) {
                            commentLineCount++;
                            inCodeLine = false;
                            break;
                        }
                        else if (line[i + 1] == '*') {
                            inCommentBlock = true;
                            commentLineCount++;
                            inCodeLine = false;
                            i++; // 遇到'*'跳过；
                        }
                    }
                    else if (c == '*' && i + 1 < line.length() && line[i + 1] == '/') {
                        inCommentBlock = false;
                        commentLineCount++;
                        inCodeLine = false;
                        i++; // 遇到'/'跳过；
                    }
                }

                if (inCodeLine) {
                    codeLineCount++;
                }
            }
        }

        file.close();

        switch (option) {
        case 'c':
            cout << "Character count: " << charCount << endl;
            break;
        case 'w':
            cout << "Word count: " << wordCount << endl;
            break;
        case 's':
            cout << "Sentence count: " << sentenceCount << endl;
            break;
        case 'd':
            cout << "Code line count: " << codeLineCount << endl;
            break;
        case 'k':
            cout << "Empty line count: " << emptyLineCount << endl;
            break;
        case 'z':
            cout << "Comment line count: " << commentLineCount << endl;
            break;
        case 'a':
            cout << "Character count: " << charCount << endl;
            cout << "Word count: " << wordCount << endl;
            cout << "Sentence count: " << sentenceCount << endl;
            cout << "Code line count: " << codeLineCount << endl;
            cout << "Empty line count: " << emptyLineCount << endl;
            cout << "Comment line count: " << commentLineCount << endl;
            break;
        default:
            cout << "Invalid option. Please enter a valid option." << endl;
            break;
        }
    }

    return 0;
}
