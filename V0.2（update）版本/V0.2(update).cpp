﻿#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    bool exitProgram = false;

    while (!exitProgram) {
        string filename;

        cout << "Enter the filename (or type 'exit' to quit): ";
        cin >> filename;

        if (filename == "exit") {
            exitProgram = true;
            continue;
        }

        ifstream file(filename);

        if (!file) {
            cout << "Error: Unable to open the file." << endl;
            continue;
        }

        string line;
        int charCount = 0;
        int wordCount = 0;

        while (getline(file, line)) {
            for (size_t i = 0; i < line.length(); i++) {
                char c = line[i];
                charCount++;

                if (c == ' ' || c == '\t') {
                    wordCount++;
                }
            }
        }

        wordCount++;

        file.close();

        cout << "Character count: " << charCount << endl;
        cout << "Word count: " << wordCount << endl;
    }

    return 0;
}
