#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    string filename;

    cout << "Enter the filename: ";
    cin >> filename;

    ifstream file(filename);

    if (!file) {
        cout << "Error: Unable to open the file." << endl;
        return 1; //若文件不能被打开，则退出程序；
    }

    string line;
    int charCount = 0;
    int wordCount = 0;

    while (getline(file, line)) {
        for (size_t i = 0; i < line.length(); i++) {
            char c = line[i];
            charCount++;

            if (c == ' ' || c == '\t') {
                // 空格或制表符表示单词边界；
                wordCount++;
            }
        }
    }

    // 为文件的最后字符+1；
    wordCount++;

    file.close();

    cout << "Character count: " << charCount << endl;
    cout << "Word count: " << wordCount << endl;

    return 0;
}
